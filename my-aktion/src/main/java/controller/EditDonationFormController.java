package controller;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import data.CampaignProducer;
import model.FormConfig;

@SessionScoped
@Named
public class EditDonationFormController implements Serializable {
	private static final long serialVersionUID = 2332342323198427L;
	
	FormConfig formConfig = new FormConfig();
	
	public FormConfig getFormConfig() {
		return formConfig;
	}

	public void setFormConfig(FormConfig formConfig) {
		this.formConfig = formConfig;
	}

	@Inject
	private CampaignProducer campaignProducer;
	
	public String doOk () {
		return Pages.LIST_CAMPAIGNS;
	}
	
	private String getAppUrl () {
		HttpServletRequest req = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();	
		String schema = req.getScheme();
		String serverName = req.getServerName();
		int serverPort = req.getServerPort();
		String contextPath = req.getContextPath();
		return schema + "://" + serverName + ":" + serverPort + contextPath;
	}
	
	public String getUrl () {
		return getAppUrl() + "/" + Pages.DONATE_MONEY + ".jsf" + "?bgColor="+formConfig.getBgColor()+"&textColor="+formConfig.getTextColor()
			+"&title="+formConfig.getTitle()+"&campaignId="+campaignProducer.getSelectedCampaign().getId();
	}
	
}
