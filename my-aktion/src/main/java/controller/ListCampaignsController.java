package controller;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import data.CampaignProducer;
import model.Campaign;

@SessionScoped
@Named
public class ListCampaignsController implements Serializable{

	private static final long serialVersionUID = 849438534542432L;
	
	@Inject
	private CampaignProducer campaignProducer;
	private Campaign campaignToDelete;
	
	public void doDeleteCampaign(Campaign campaign) {
		this.campaignToDelete = campaign;
		System.out.println("Mark Campaign for delete");
	}
	
	public void commitDeleteCampaign () {
		System.out.println("Delete not implemented yet");
	}
	
	public String doAddCampaign () {
		campaignProducer.prepareAddCampaign();
		return Pages.EDIT_CAMPAIGN;
	}
	
	public String doEditCampaign(Campaign campaign) {
		campaignProducer.prepareEditCampaign(campaign);
		return Pages.EDIT_CAMPAIGN;
	}
	
	public String doEditDonationForm(Campaign campaign) {
		campaignProducer.setSelectedCampaign(campaign);
		return Pages.EDIT_DONATION_FORM;
	}
	
	public String doListDonations(Campaign campaign) {
		campaignProducer.setSelectedCampaign(campaign);
		return Pages.LIST_DONATIONS;
	}
	
}
