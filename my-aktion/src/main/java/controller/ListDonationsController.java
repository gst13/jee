package controller;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@SessionScoped
@Named
public class ListDonationsController implements Serializable {
	private static final long serialVersionUID = 2342322342312111L;
	
	public String doOK () {
		return Pages.LIST_CAMPAIGNS;
	}

}
