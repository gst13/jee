package model;

public class FormConfig {

	private String textColor;
	private String bgColor;
	private String title;
	
	public FormConfig () {
		this.textColor = "000000";
		this.bgColor = "ffffff";
		this.title = "Geld spenden";
	}

	public String getTextColor() {
		return textColor;
	}
	public void setTextColor(String textColor) {
		this.textColor = textColor;
	}
	public String getBgColor() {
		return bgColor;
	}
	public void setBgColor(String bgColor) {
		this.bgColor = bgColor;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
