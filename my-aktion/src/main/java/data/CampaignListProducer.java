package data;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import model.Account;
import model.Campaign;
import model.Donation;
import model.Donation.Status;

@SessionScoped
@Named
public class CampaignListProducer implements Serializable {
	public static final long serialVersionUID = -2678624234234L;
	
	private List<Campaign> campaigns;
	
	public CampaignListProducer() {
		campaigns = createMockCampaigns();
	}
	
	public List<Campaign> getCampaigns () {
		return campaigns;
	}
	
	public List<Campaign> createMockCampaigns() {
		Donation donation1 = new Donation();
		donation1.setDonorName("Heinz Schmitt");
		donation1.setAmount(20d);
		donation1.setReceiptRequested(true);
		donation1.setStatus(Status.TRANSFERRED);
		donation1.setAccount(new Account(donation1.getDonorName(),"XBank","DE12312"));
		Donation donation2 = new Donation();
		donation2.setDonorName("Karl Meier");
		donation2.setAmount(30d);
		donation2.setReceiptRequested(false);
		donation2.setStatus(Status.IN_PROCESS);
		donation2.setAccount(new Account(donation1.getDonorName(),"XYBank","DE34212312"));
		List<Donation> spenden = new LinkedList<>();
		spenden.add(donation1);
		spenden.add(donation2);
		Campaign campaign1 = new Campaign();
		campaign1.setName("Trikots für A-Jugend");
		campaign1.setTargetAmount(1000d);
		campaign1.setAmountDonatedSoFar(258d);
		campaign1.setDonationMinimum(20d);
		campaign1.setId(1L);
		campaign1.setAccount(new Account("Max Mustermann","Sparkasse","AT34245454544"));
		campaign1.setDonations(spenden);
		Campaign campaign2 = new Campaign();
		campaign2.setName("Rollstuhl für Maria");
		campaign2.setTargetAmount(2500d);
		campaign2.setAmountDonatedSoFar(742d);
		campaign2.setDonationMinimum(25d);
		campaign2.setId(2L);
		campaign2.setAccount(campaign1.getAccount());
		campaign2.setDonations(spenden);
		List<Campaign> ret = new LinkedList<>();
		ret.add(campaign1);
		ret.add(campaign2);
		return ret;
	}
}
