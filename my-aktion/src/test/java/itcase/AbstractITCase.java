package itcase;

import java.io.File;

import org.jboss.shrinkwrap.api.importer.ZipImporter;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.arquillian.container.test.api.Deployment;

public class AbstractITCase {
	@Deployment(testable=false)
	public static WebArchive createDeployment (){
		return ShrinkWrap.create(ZipImporter.class,"test.war").importFrom(new File("target/my-aktion.war")).as(WebArchive.class);
	}

}
